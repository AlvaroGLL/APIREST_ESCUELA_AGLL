package grupocmc.alvaro.apiRestEscuela.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import grupocmc.alvaro.apiRestEscuela.enums.Curso;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(exclude = "asignaturas")
@ToString
@Entity
@Table(name = "matriculas")
public class Matricula {

	@Id
	@Column(name = "matricula_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NonNull
	@Column(name = "importe")
	private Double importe;

	@NonNull
	@Column(name = "curso")
	@Enumerated(EnumType.STRING)
	private Curso curso;

	@ManyToOne
	@JoinColumn(name = "alumno_id", referencedColumnName = "alumno_id")
	@JsonBackReference(value = "matricula_alumno")
	private Alumno alumno;
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
	@JoinTable(name = "matricula_asignatura", 
		joinColumns = @JoinColumn(name = "matricula_id", referencedColumnName = "matricula_id"), 
		inverseJoinColumns = @JoinColumn(name = "asignatura_id", referencedColumnName = "asignatura_id"))
	private Set<Asignatura> asignaturas = new HashSet<>();

	public Matricula(Double importe, Curso curso, Set<Asignatura> asignaturas) {
		this.importe = importe;
		this.curso = curso;
		this.asignaturas = asignaturas;
	}

}
