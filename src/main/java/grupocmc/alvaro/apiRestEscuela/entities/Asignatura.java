package grupocmc.alvaro.apiRestEscuela.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import grupocmc.alvaro.apiRestEscuela.enums.Curso;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "asignaturas")
public class Asignatura {
	
	@Id
	@Column(name = "asignatura_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NonNull
	@Column(name = "nombre")
	private String nombre;
	
	@NonNull
	@Column(name = "precio")
	private Double precio;
	
	@NonNull
	@Column(name = "maximo_alumnos")
	private Integer maximoAlumnos;
	
	@NonNull
	@Column(name = "curso")
	@Enumerated(EnumType.STRING)
	private Curso curso;
	
	@ManyToMany(mappedBy = "asignaturas", fetch= FetchType.LAZY)
	@JsonBackReference(value = "asignatura_matriculas")
	private Set<Matricula> matriculas = new HashSet<>();
	
}
