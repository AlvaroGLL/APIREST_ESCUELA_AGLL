package grupocmc.alvaro.apiRestEscuela.exceptions;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class ExcepcionPersonalizada extends Exception {

	private static final long serialVersionUID = 1L;

	@NonNull
	private String mensajeError;

	private boolean hayObjetoExcepcion = false;

	// CONSTRUCTORES
	public ExcepcionPersonalizada(Exception error) {
		super(error);
		this.hayObjetoExcepcion = true;
	}

	public ExcepcionPersonalizada(String mensajeError, Exception error) {
		super(error);
		this.mensajeError = mensajeError;
		this.hayObjetoExcepcion = true;

	}

	// Una suerte de toString
	public void mostrarError() {
		if (hayObjetoExcepcion) {
			log.error(mensajeError + " ----> " + this.getMessage());
		} else {
			log.error(mensajeError);
		}

	}

	// GETTERS & SETTERS
	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

}
