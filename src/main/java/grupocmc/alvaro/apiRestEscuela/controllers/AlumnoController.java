package grupocmc.alvaro.apiRestEscuela.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupocmc.alvaro.apiRestEscuela.services.IAlumnoService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/alumnos")
public class AlumnoController {
	
	@Autowired
	private IAlumnoService alumnoService;
	
	/**
	 * Consulta todos los alumnos.
	 * @return JSON con todos los alumnos.
	 */
	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Obtiene un listado de todos los alumnos.")
	public String obtenerTodos() {
		return alumnoService.obtenerTodos();
	}
	
	/**
	 * Busca un alumnop dada su id.
	 * @param id El id del alumno a buscar.
	 * @return Devuelve el alumno en JSON si lo encuentra.
	 */
	@GetMapping(value = "/alumno", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Obtiene un alumno concreto dado por id a través de parámetro.")
	public String encontrarPorId(@RequestParam("id") Long id) {
		return alumnoService.encontrarPorId(id);
	}
	
	/**
	 * Recibe un alumno en JSON y lo guarda si es correcto.
	 * @param contenido El alumno en JSON
	 * @return El alumno si se ha guardado.
	 */
	@PostMapping(value = "/alumno", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Recibe un JSON que guarda como entrada en el repositorio si está bien formado.")
	public String guardar(@RequestBody String contenido) {
		return alumnoService.guardar(contenido);
	}
	
	/**
	 * Actualiza, si existe, el alumno dado.
	 * @param contenido El alumno en JSON.
	 * @return El alumno actualizado si no se han hallado conflictos.
	 */
	@PutMapping(value = "/alumno", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Actualiza el alumno dado en JSON.")
	public String actualizar(@RequestBody String contenido) {
		return alumnoService.actualizar(contenido);
	}
	
	/**
	 * Borra un alumno dada su id.
	 * @param id La id del alumno.
	 */
	@DeleteMapping(value = "/alumno")
	@ApiOperation("Borra un alumno dada su id.")
	public String borrarPorId(@RequestParam("id") Long id) {
		return alumnoService.borrarPorId(id);
	}
	
	@PostMapping(value="alumno/{alumnoId}/matricula",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Guarda si todo es correcto una matrícula en alumno.")
	public String crearMatricula(@PathVariable Long alumnoId, @RequestBody String contenido) {
		return alumnoService.crearMatricula(alumnoId, contenido);
	}

}
