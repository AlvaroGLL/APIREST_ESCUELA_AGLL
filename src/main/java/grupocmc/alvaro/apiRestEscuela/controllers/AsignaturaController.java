package grupocmc.alvaro.apiRestEscuela.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import grupocmc.alvaro.apiRestEscuela.services.IAsignaturaService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/asignaturas")
public class AsignaturaController {

	@Autowired
	private IAsignaturaService asignaturaService;
	
	/**
	 * Devuelve un listado de alumnos por matrícula si ésta existe.
	 * @param asignaturaId El id de la asignatura.
	 * @return Listado de alumnos por asignatura.
	 */
	@GetMapping(value = "/{asignaturaId}/alumnos", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Devuelve un listado de alumnos por asignatura si es que esta asignatura existe.")
	public String encontrarAlumnosPorAsignatura(@PathVariable Long asignaturaId) {
		return asignaturaService.encontrarAlumnosPorAsignatura(asignaturaId);
	}
}
