package grupocmc.alvaro.apiRestEscuela.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupocmc.alvaro.apiRestEscuela.services.IMatriculaService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/matriculas")
public class MatriculaController {

	@Autowired
	private IMatriculaService matriculaService;
	
	/**
	 * Borra una matrícula dada su id.
	 * @param id El id de la matrícula a borrar.
	 * @return OK o KO
	 */
	@DeleteMapping(value = "/matricula")
	@ApiOperation("Borra una matrícula dada su id.")
	private String borrarPorId(@RequestParam("id") Long id) {
		return matriculaService.borrarPorId(id);
	}
	
}
