package grupocmc.alvaro.apiRestEscuela.transformers;

public interface JsonTransformer {
	
	String toJson(Object data);
	Object fromJson(String json, @SuppressWarnings("rawtypes") Class clazz);

}
