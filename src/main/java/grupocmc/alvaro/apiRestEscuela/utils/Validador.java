package grupocmc.alvaro.apiRestEscuela.utils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Stream;

import grupocmc.alvaro.apiRestEscuela.exceptions.ExcepcionPersonalizada;

public  class Validador {
	
	/**
	 * Valida los campos de un objeto para que no puedan ser nulos, salvo ID.
	 * @param object Objeto a validar.
	 * @return True si todo ok.
	 */
	public static boolean validarEntidadNoId(Object object) {
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
		}

		Boolean validado = Stream.of(fields)
				.filter(field -> !field.getName().equals("id") && !field.isSynthetic())
		        .noneMatch(f -> {
					try {
						return f.get(object) == null;
					} catch (IllegalArgumentException | IllegalAccessException e) {
						ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Ha ocurrido un error accediendo a los campos del objeto en el validador.", e);
						ep.getMensajeError();
					}
					return false;
				});
		
		return validado;

	}
	
	
	/**
	 * Valida campos nulos de un objeto excluyendo los pasados por una lista.
	 * @param object El objeto del cual vamos a validar sus campos.
	 * @param atributos La lista de strings con los nombres de los campos a excluir.
	 * @return TRUE si ninguno de los campos analizados es nulo. False si alguno nulo.
	 */
	public static boolean validarEntidadParametros(Object object, List<String> atributos) {
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
		}
		
		Boolean validado = Stream.of(fields)
				.filter(field -> !atributos.contains(field.getName()) && !field.isSynthetic())
		        .noneMatch(f -> {
					try {
						return f.get(object) == null;
					} catch (IllegalArgumentException | IllegalAccessException e) {
						ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Ha ocurrido un error accediendo a los campos del objeto en el validador.", e);
						ep.getMensajeError();
					}
					return false;
				});
		
		return validado;
		
	}

}
