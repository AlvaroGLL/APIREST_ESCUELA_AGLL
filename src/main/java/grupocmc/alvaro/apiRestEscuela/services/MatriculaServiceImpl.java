package grupocmc.alvaro.apiRestEscuela.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import grupocmc.alvaro.apiRestEscuela.exceptions.ExcepcionPersonalizada;
import grupocmc.alvaro.apiRestEscuela.repositories.MatriculaRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MatriculaServiceImpl implements IMatriculaService {
	
	@Autowired
	private MatriculaRepository matriculaRepository;
	
	private final String NO_EXISTE = "Elemento no encontrado.";
	
	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IMatriculaService#borrarPorId(java.lang.Long)
	 */
	@Override
	public String borrarPorId(Long id) {
		log.debug("ENTRANDO EN borrarPorId DE MatriculaService");
		try {
			log.debug("BORRANDO . . .");
			matriculaRepository.deleteById(id);
		} catch (EmptyResultDataAccessException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada(NO_EXISTE, ex);
			ep.mostrarError();
			return NO_EXISTE;			
		} catch(IllegalArgumentException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("El id no puede ser nulo.", ex);
			ep.mostrarError();
			return "Id no puede ser nulo.";
		} catch (Exception ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Excepción de Java.", ex);
			ep.mostrarError();
			return "KO";
		}
		
		return "OK";
	}

}
