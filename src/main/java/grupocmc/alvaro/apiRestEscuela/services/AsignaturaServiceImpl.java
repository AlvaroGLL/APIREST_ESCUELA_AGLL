package grupocmc.alvaro.apiRestEscuela.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import grupocmc.alvaro.apiRestEscuela.entities.Alumno;
import grupocmc.alvaro.apiRestEscuela.entities.Asignatura;
import grupocmc.alvaro.apiRestEscuela.exceptions.ExcepcionPersonalizada;
import grupocmc.alvaro.apiRestEscuela.repositories.AsignaturaRepository;
import grupocmc.alvaro.apiRestEscuela.transformers.JsonTransformer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AsignaturaServiceImpl implements IAsignaturaService {

	private AsignaturaRepository asignaturaRepository;
	private JsonTransformer jsonTransformer;
	
	public AsignaturaServiceImpl(AsignaturaRepository asignaturaRepository, JsonTransformer jsonTransformer) {
		this.asignaturaRepository = asignaturaRepository;
		this.jsonTransformer = jsonTransformer;
	}
	
	private final String NO_EXISTE = "Elemento no encontrado.";
	
	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IAsignaturaService#encontrarAlumnosPorAsignatura(java.lang.Long)
	 */
	@Override
	public String encontrarAlumnosPorAsignatura(Long id) {
		Optional<Asignatura> aOpt = asignaturaRepository.findById(id);
		
		if(!aOpt.isPresent()) {
			log.debug("ESTA ASIGNATURA NO EXISTE");
			return NO_EXISTE;
		}
		
		Asignatura asignatura = aOpt.get();
		List<Alumno> alumnos = new ArrayList<>();
		try {
			asignatura.getMatriculas().forEach(m -> alumnos.add(m.getAlumno()));
		} catch (NullPointerException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Lista de matrículas nula.", ex);
			ep.mostrarError();
		} 
		
		return jsonTransformer.toJson(alumnos);
	}
	
}
