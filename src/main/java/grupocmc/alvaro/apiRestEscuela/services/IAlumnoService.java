package grupocmc.alvaro.apiRestEscuela.services;

public interface IAlumnoService {

	/**
	 * Consulta todos los alumnos del repositorio.
	 * 
	 * @return JSON con los alumnos
	 */
	String obtenerTodos();

	/**
	 * Encuentra un alumno dada una id.
	 * 
	 * @param id La id a buscar.
	 * @return Devuelve el alumno en JSON si lo encuentra. Si no, indica lo
	 *         contrario.
	 */
	String encontrarPorId(Long id);

	/**
	 * Guarda un alumno en el repositorio si está correcto.
	 * @param contenido Alumno en formato JSON.
	 * @return El alumno guardado si la operación ha tenido éxito.
	 */
	String guardar(String contenido);

	/**
	 * Actualiza, si existe, un autor dado.
	 * @param contenido El autor en JSON.
	 * @return El objeto actualizado si existe.
	 */
	String actualizar(String contenido);

	/**
	 * Borra un alumno dada su id.
	 * @param id El id delalumno a borrar.
	 */
	String borrarPorId(Long id);

	/**
	 * Recibe una matrícula un alumno.
	 * Si el alumno no está matriculado en ese curso, las asignaturas son del curso de la matrícula, 
	 * no se ha alcanzado el número máximo de alumnos y la asignatura es válida y matrícula tiene asignaturas,
	 * actualiza el precio de la matrícula y la insertaen alumno.
	 * @param alumnoId El id del alumno.
	 * @param contenido La matrícula en JSON.
	 * @return La matrícula si la guarda.
	 */
	String crearMatricula(Long alumnoId, String contenido);

}