package grupocmc.alvaro.apiRestEscuela.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import grupocmc.alvaro.apiRestEscuela.entities.Alumno;
import grupocmc.alvaro.apiRestEscuela.entities.Asignatura;
import grupocmc.alvaro.apiRestEscuela.entities.Matricula;
import grupocmc.alvaro.apiRestEscuela.exceptions.ExcepcionPersonalizada;
import grupocmc.alvaro.apiRestEscuela.repositories.AlumnoRepository;
import grupocmc.alvaro.apiRestEscuela.repositories.AsignaturaRepository;
import grupocmc.alvaro.apiRestEscuela.transformers.JsonTransformer;
import grupocmc.alvaro.apiRestEscuela.utils.Validador;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlumnoServiceImpl implements IAlumnoService {

	@Autowired
	private AlumnoRepository alumnoRepository;
	
	@Autowired
	private AsignaturaRepository asignaturaRepository;
	
	@Autowired
	private JsonTransformer jsonTransformer;
	
	
	private final String JSON_ERROR = "JSON incorrecto.";
	private final String JSON_INCOMPLETO = "JSON incompleto.";
	private final String NO_EXISTE = "Elemento no encontrado.";
	private final String SI_EXISTE = "El elemento ya se encuentra en el repositorio.";
	private final String NO_ALTA = "Imposible dar de alta.";

	
	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IAlumnoService#obtenerTodos()
	 */
	@Override
	public String obtenerTodos() {
		log.debug("ENTRANDO EN obtenerTodos DE AlumnoService");
		return jsonTransformer.toJson(alumnoRepository.findAll());
	}

	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IAlumnoService#encontrarPorId(java.lang.Long)
	 */
	@Override
	public String encontrarPorId(Long id) {
		log.debug("ENTRANDO EN encontrarPorId DE AlumnoService");
		Optional<Alumno> aOpt = alumnoRepository.findById(id);
		if (aOpt.isPresent()) {
			log.debug("ALUMNO ENCONTRADO.");
			return jsonTransformer.toJson(aOpt.get());
		} else {
			log.debug("ALUMNO NO ENCONTRADO.");
			return NO_EXISTE;
		}
	}

	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IAlumnoService#guardar(java.lang.String)
	 */
	@Override
	public String guardar(String contenido) {
		log.debug("ENTRANDO EN guardar DE AlumnoService");
		
		try {
			Alumno alumno = (Alumno) jsonTransformer.fromJson(contenido, Alumno.class);
			List<String> parametrosAlumno = Arrays.asList("id, direccion", "matriculas");
			
			if(alumno.getId() != null) {
				if(alumnoRepository.findById(alumno.getId()).isPresent()) {
					log.debug("ESTE ALUMNO YA EXISTE");
					return SI_EXISTE;
				}
			}
			
			
			if(!alumno.getMatriculas().isEmpty()) {
				log.debug("ALUMNO NO PUEDE SER DADO DE ALTA CON MATRICULA.");
				return JSON_ERROR;
			}

			if (Validador.validarEntidadParametros(alumno, parametrosAlumno)) {
				log.debug("ALUMNO VALIDADO");
				
				if (alumno.getDireccion() == null) {
					log.debug("GUARDANDO ALUMNO SIN DIRECCION . . . ");
					return jsonTransformer.toJson(alumnoRepository.save(alumno));
					
				} else if (Validador.validarEntidadNoId(alumno.getDireccion())) {
					log.debug("ALUMNO VALIDADO");
					log.debug("GUARDANDO ALUMNO CON DIRECCION . . .");
					return jsonTransformer.toJson(alumnoRepository.save(alumno));
					
				} else {
					log.debug("JSON INCOMPLETO");
					return JSON_INCOMPLETO;
				}

			} else {
				log.debug("JSON INCOMPLETO");
				return JSON_INCOMPLETO;
			}

		} catch(IllegalArgumentException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("El id no puede ser nulo.", ex);
			ep.mostrarError();
			return "Id no puede ser nulo.";
		} catch (Exception ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Ha habido un error con JSON.", ex);
			ep.mostrarError();
		}
		
		log.debug("JSON ERRONEO");
		return JSON_ERROR;
	}
	
	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IAlumnoService#actualizar(java.lang.String)
	 */
	@Override
	public String actualizar (String contenido) {
		log.debug("ENTRANDO EN actualizar DE AlumnoService");
		
		try {
			Alumno alumno = (Alumno) jsonTransformer.fromJson(contenido, Alumno.class);
			List<String> parametrosAlumno = Arrays.asList("id", "matriculas", "direccion");
			Optional<Alumno> aOpt = alumnoRepository.findById(alumno.getId());
			
			if(!aOpt.isPresent()) {
				log.debug("ESTE ALUMNO NO EXISTE");
				return NO_EXISTE;
			}

			if (Validador.validarEntidadParametros(alumno, parametrosAlumno)) {
				log.debug("ALUMNO VALIDADO");
				
				if (alumno.getDireccion() == null) {
					log.debug("GUARDANDO ALUMNO SIN DIRECCION, BORRANDO DIRECCION SI EXISTE . . . ");
					return jsonTransformer.toJson(alumnoRepository.save(alumno));
					
				} else if (Validador.validarEntidadNoId(alumno.getDireccion())) {
					log.debug("ALUMNO VALIDADO");
					log.debug("GUARDANDO ALUMNO CON DIRECCION . . .");
					return jsonTransformer.toJson(alumnoRepository.save(alumno));
					
				} else {
					log.debug("JSON INCOMPLETO");
					return JSON_INCOMPLETO;
				}

			} else {
				log.debug("JSON INCOMPLETO");
				return JSON_INCOMPLETO;
			}

		} catch(IllegalArgumentException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("El id no puede ser nulo.", ex);
			ep.mostrarError();
			return "Id no puede ser nulo.";
		} catch (Exception ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Ha habido un error con JSON.", ex);
			ep.mostrarError();
		}
		
		log.debug("JSON ERRONEO");
		return JSON_ERROR;
	}
	
	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IAlumnoService#borrarPorId(java.lang.Long)
	 */
	@Override
	public String borrarPorId(Long id) {
		log.debug("ENTRANDO EN borrarPorId DE AlumnoService");
		try {
			log.debug("BORRANDO . . .");
			alumnoRepository.deleteById(id);
		} catch (EmptyResultDataAccessException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada(NO_EXISTE, ex);
			ep.mostrarError();
			return NO_EXISTE;			
		} catch(IllegalArgumentException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("El id no puede ser nulo.", ex);
			ep.mostrarError();
			return "Id no puede ser nulo.";
		} catch (Exception ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Excepción de Java.", ex);
			ep.mostrarError();
			return "KO";
		}
		
		return "OK";
	}
	
	
	/* (non-Javadoc)
	 * @see grupocmc.alvaro.apiRestEscuela.services.IAlumnoService#crearMatricula(java.lang.Long, java.lang.String)
	 */
	@Override
	public String crearMatricula(Long alumnoId, String contenido) {
		log.debug("ENTRANDO EN crearMatricula DE AlumnoService");
		
		try {
			Matricula m = (Matricula) jsonTransformer.fromJson(contenido, Matricula.class);
			List<String> parametrosMatricula = Arrays.asList("id", "importe", "alumno");
			Optional<Alumno> aOpt = alumnoRepository.findById(alumnoId);
			
			if(!aOpt.isPresent()) {
				log.debug("ESTE ALUMNO NO EXISTE");
				return NO_EXISTE;
			}

			if (Validador.validarEntidadParametros(m, parametrosMatricula)) {
				Alumno alumno = aOpt.get();
				log.debug("MATRICULA VALIDADA");
				
				if (m.getAlumno() != null || m.getImporte() != null) {
					log.debug("MATRÍCULA NO PUEDE TENER ALUMNO O IMPORTE POR DEFECTO, SE ASIGNAN AUTOMÁTICOS");
					return JSON_ERROR;
					
				} else if(alumno.getMatriculas().stream().noneMatch(matAl -> matAl.getCurso().equals(m.getCurso())) 
						&& m.getAsignaturas().stream().allMatch(a -> a.getCurso().equals(m.getCurso()))){
					
					m.setImporte(0.0);
					for (Asignatura asg : m.getAsignaturas()) {
						if(asg.getId() != null) {
							Optional<Asignatura> asigOpt =  asignaturaRepository.findById(asg.getId());
							if(asigOpt.isPresent() && asigOpt.get().getMatriculas().size() >= asigOpt.get().getMaximoAlumnos()) {
								m.getAsignaturas().remove(asg);
								
								if(m.getAsignaturas().isEmpty()) {
									log.debug("NO HAY ASIGNATURAS PARA HACER MATRÍCULA");
									return NO_ALTA;
								} else {
									m.setImporte(m.getImporte() + asg.getPrecio());
								}
								
							} else {
								log.debug("ASIGNATURA REFERENCIADA NO EXISTENTE O NO ADMITE MATRÍCULA");
								return JSON_ERROR;
							}
						} else {
							m.setImporte(m.getImporte() + asg.getPrecio());
						}
						
					}
					m.setAlumno(alumno);
					alumno.getMatriculas().add(m);
					return jsonTransformer.toJson(alumnoRepository.save(alumno));
					
				} else {
					log.debug("UNA MATRICULA POR CURSO. EL CURSO DE LA LA MATRICULA Y LAS ASIGNATURAS HA DE COINCIDIR");
					return NO_ALTA;
					
				}

			} else {
				log.debug("JSON INCOMPLETO");
				return JSON_INCOMPLETO;
			}

		} catch(IllegalArgumentException ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("El id no puede ser nulo.", ex);
			ep.mostrarError();
			return "Id no puede ser nulo.";
		} catch (Exception ex) {
			ExcepcionPersonalizada ep = new ExcepcionPersonalizada("Ha habido un error con JSON.", ex);
			ep.mostrarError();
		}
		
		log.debug("JSON ERRONEO");
		return JSON_ERROR;
		
	}

}
