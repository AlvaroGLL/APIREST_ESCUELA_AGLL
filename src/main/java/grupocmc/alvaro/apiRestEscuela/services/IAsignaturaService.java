package grupocmc.alvaro.apiRestEscuela.services;

public interface IAsignaturaService {

	/**
	 * Devuelve un listado de los alumnos por asignatura si la asignatura existe.
	 * @param id El id de la asignatura.
	 * @return Listado de alumnos en JSON si se encuentra.
	 */
	String encontrarAlumnosPorAsignatura(Long id);

}