package grupocmc.alvaro.apiRestEscuela.services;

public interface IMatriculaService {

	/**
	 * Elimina una matrícula.
	 * @param id El id de la matrícula.
	 * @return OK o KO
	 */
	String borrarPorId(Long id);

}