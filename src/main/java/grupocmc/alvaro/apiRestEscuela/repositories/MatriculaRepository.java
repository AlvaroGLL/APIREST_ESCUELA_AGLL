package grupocmc.alvaro.apiRestEscuela.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import grupocmc.alvaro.apiRestEscuela.entities.Matricula;

@Repository
public interface MatriculaRepository extends JpaRepository<Matricula, Long> {

}
