package grupocmc.alvaro.apiRestEscuela.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import grupocmc.alvaro.apiRestEscuela.entities.Alumno;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Long>{

}
