package grupocmc.alvaro.apiRestEscuela;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApirestEscuelaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApirestEscuelaApplication.class, args);
	}

}
