-- DIRECCIONES
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (1, 'Calle', 'Caleruega 81');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (2, 'Calle', 'Flores 9');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (3, 'Calle', 'Proudhon 12');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (4, 'Plaza', 'España 4');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (5, 'Calle', 'Ceudas 6');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (6, 'Calle', 'Fantasía 90');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (7, 'Calle', 'Rigoberta2');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (8, 'Avenida', 'Retamas 321');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (9, 'Avenida', 'Cuba');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (10, 'Calle', 'Cero dramas');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (11, 'Calle', 'Siempre smile');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (12, 'Calle', 'Sciopero');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (13, 'Calle', 'Galicia');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (14, 'Calle', 'Mahou');
INSERT INTO direcciones (direccion_id, tipo_via, direccion) values (15, 'Calle', 'Estrella');

-- ALUMNOS
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (1, 'Álvaro 9000', 1);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (2, 'Carlos Guay', 2);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (3, 'Noel Cool', 3);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (4, 'Daniel Karate', 4);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (5, 'Daniel No-Karate', 5);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (6, 'Juanma Dam', 6);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (7, 'Alba', 7);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (8, 'Kimberly', 8);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (9, 'John', 9);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (10, 'Hen', 10);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (11, 'Cheester', 11);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (12, 'Laura', 12);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (13, 'Sophia', 13);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (14, 'Loubna', 14);
INSERT INTO alumnos (alumno_id, nombre, direccion_id) VALUES (15, 'Ernesto', 15);


--ASIGNATURAS
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(1, 'PRIMERO', 3, 'Metafísica', 248.5);
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(2, 'PRIMERO', 4, 'Filosofía del Lenguaje', 123.1);
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(3, 'SEGUNDO', 3, 'Estética', 212.9);
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(4, 'SEGUNDO', 5, 'Historia de la Filosofía I', 100.0);
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(5, 'TERCERO', 6, 'Lógica y Argumentación', 298.2);
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(6, 'TERCERO', 4, 'Ética', 189.2);
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(7, 'CUARTO', 6, 'Análisis de Poder', 312.6);
INSERT INTO asignaturas (asignatura_id, curso, maximo_alumnos, nombre, precio) VALUES(8, 'CUARTO', 2, 'Teorías de la Libertad', 270.6);

-- MATRICULAS
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (1, 'PRIMERO', null, null);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (2, 'PRIMERO', 371.6, 1);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (3, 'SEGUNDO', 312.9, 2);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (4, 'SEGUNDO', 100.0, 3);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (5, 'TERCERO', 298.2, 4);

INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (6, 'TERCERO', 487.4, 5);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (7, 'TERCERO', 487.4, 6);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (8, 'CUARTO', 312.6, 7);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (9, 'CUARTO', 312.6, 9);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (10, 'TERCERO', 487.4, 2);

INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (11, 'PRIMERO', 371.6, 3);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (12, 'SEGUNDO', 100, 1);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (13, 'TERCERO',  189.2, 1);
INSERT INTO matriculas (matricula_id, curso, importe, alumno_id) VALUES (14, 'CUARTO', 312.6, 1);

-- MATRICULA_ASIGNATURA
INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (2, 1);
INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (2, 2);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (3, 3);
INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (3, 4);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (4, 4);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (5, 5);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (6, 5);
INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (6, 6);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (7, 5);
INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (7, 6);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (8, 7);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (9, 7);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (10, 5);
INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (10, 6);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (11, 1);
INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (11, 2);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (12, 5);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (13, 6);

INSERT INTO matricula_asignatura (matricula_id, asignatura_id) VALUES (14, 7);



