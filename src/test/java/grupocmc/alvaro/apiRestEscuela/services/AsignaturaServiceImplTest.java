package grupocmc.alvaro.apiRestEscuela.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import grupocmc.alvaro.apiRestEscuela.entities.Asignatura;
import grupocmc.alvaro.apiRestEscuela.enums.Curso;
import grupocmc.alvaro.apiRestEscuela.repositories.AsignaturaRepository;
import grupocmc.alvaro.apiRestEscuela.transformers.JsonTransformer;
import grupocmc.alvaro.apiRestEscuela.transformers.JsonTransformerImp;

@RunWith(MockitoJUnitRunner.class)
public class AsignaturaServiceImplTest {
	
	@InjectMocks
	private static IAsignaturaService asignaturaService;
	
	private static JsonTransformer jsonTransformer;
	
	@Mock
	private static AsignaturaRepository asignaturaRepository;
	
	private static Set<Asignatura> asignaturas = new HashSet<>();
	private static Optional<Asignatura> a;
	
	@BeforeClass
	public static void setUp() {
		jsonTransformer = new JsonTransformerImp(); 
		asignaturaService = new AsignaturaServiceImpl(asignaturaRepository, jsonTransformer);
		Asignatura a1 = new Asignatura();
		Asignatura a2 = new Asignatura();
		Asignatura a3 = new Asignatura("mates", 10.0, 4, Curso.PRIMERO);
		a3.setId(3L);
		a = Optional.of(a3);
		
		a1.setId(1L);
		a1.setId(2L);
		
		asignaturas.add(a1);
		asignaturas.add(a2);
		
	}
	
	
	

	@Test
	public void testEncontrarAlumnosPorAsignaturaOk() throws Exception{
		Long id = 3L;
		//Asignatura asig = new Asignatura();
		//asig.setId(id);
		//a = Optional.of(asig);
		
		System.out.println(a.get());
		System.out.println(jsonTransformer.toJson(a.get()));
		when(asignaturaRepository.findById(id)).thenReturn(a);
		
		ArgumentCaptor<Asignatura> argumentCaptor = ArgumentCaptor.forClass(Asignatura.class);
		
		String expected = jsonTransformer.toJson(a.get()).toString();
		
		String actual = asignaturaService.encontrarAlumnosPorAsignatura(id);
		
		JSONAssert.assertEquals(expected, actual, JSONCompareMode.LENIENT);
		
		argumentCaptor.getValue();
		verify(asignaturaRepository, times(1)).findById(id);
		
	}

}
