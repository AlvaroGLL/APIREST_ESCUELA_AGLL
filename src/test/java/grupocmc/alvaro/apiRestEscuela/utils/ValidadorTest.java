package grupocmc.alvaro.apiRestEscuela.utils;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import grupocmc.alvaro.apiRestEscuela.entities.Alumno;
import grupocmc.alvaro.apiRestEscuela.entities.Direccion;

public class ValidadorTest {

	private Alumno alumno;
	private Direccion direccion;
	
	@Before
	public void cargarAlumno() {
		alumno = new Alumno("Carlos");
		direccion = new Direccion("Calle", "Callosa 1");
	}
	
	
	@Test
	public void testValidarEntidadIdNullTrue() {
		assertTrue(Validador.validarEntidadNoId(direccion));
	}
	
	@Test
	public void testValidarEntidadIdNullFalse() {
		assertFalse(Validador.validarEntidadNoId(alumno));
	}
	
	@Test
	public void validarPorAtributosTrue() {
		List<String> atributos = Arrays.asList("id", "matriculas");
		alumno.setId(null);
		alumno.setDireccion(direccion);
		alumno.setMatriculas(null);
		assertTrue(Validador.validarEntidadParametros(alumno, atributos));
	}
	
	@Test
	public void validarPorAtributosFalse() {
		List<String> atributos = Arrays.asList("id", "direccion");
		alumno.setId(null);
		alumno.setDireccion(null);
		alumno.setMatriculas(null);
		assertFalse(Validador.validarEntidadParametros(alumno, atributos));
	}

}
